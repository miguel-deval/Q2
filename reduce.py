#!/usr/bin/python

"""
Reduction of C/2014 Q2 (Lovejoy) data

HCN, H2CO, CO, CH3OH
"""

from pyspeckit.spectrum.readers.read_class import read_class
from os.path import expanduser, join
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from hsso.class_utils import frequency
from hsso import gildas
from astropy.modeling import models, fitting
from astropy.time import Time
import numpy as np
from astropy import units as u
import astropy.coordinates as coords
import argparse
from helcorr import helcorr
from scipy.constants import constants
import sqlite3
import callhorizons

# comet ID may change in the future in Horizons!
horizons = callhorizons.query('904109')

def obslog(df):
    colors = cm.rainbow(np.linspace(0, 1, 4))
    for c, li in zip(colors, ('HCN', 'CO', 'H2CO', 'CH3OH')):
        x = df[df['XLINE'].str.startswith(li.encode('utf-8'))]['OBSDATE']
        plt.scatter(x, np.ones(len(x)), color=c, label=li)
    x = df['OBSDATE'].values
    d = x[-1] - x[0]
    plt.xlim(x[0]-d/50, x[-1]+d/50)
    plt.axvline(57039.2)
    plt.legend()
    plt.show()
    plt.close()


def ephemeris(df):
    x = df['OBSDATE']
    horizons.set_discreteepochs(2400000.5+x.values[[0,-1]])
    horizons.get_ephemerides(-7)
    print(horizons['r'])
    print(horizons['delta'])


# parse command-line arguments
parser = argparse.ArgumentParser()
parser.add_argument('-t', '--tel', choices=('X201', 'X202'), default='X201',
                    help='Telescope')
parser.add_argument('-l', '--line', default='HCN',
                    choices=('HCN', 'CO', 'H2CO', 'H2CO(3-2)', 'CH3OH'),
                    help='Line name')
parser.add_argument('-s', '--source', default='^C2014Q2', help='Source name (default is ^C2014Q2)')
parser.add_argument('-x', default='f', choices=('f', 'v'), help='x axis')
parser.add_argument('--rebin', default=1, type=int, help='rebin factor')
args = parser.parse_args()

# line frequencies
freq0 = {'HCN': 265.8864339,
         # 'CO': 229.75,
         'CO': 230.538,
         'H2CO': 225.69778,
         # 'CH3OH': 241.904645}
         'CH3OH': 241.79143}
freq0['H2CO(3-2)'] = freq0['H2CO']

spectra = []
header = []
datadir = expanduser('~/project/Q2/data/')

# read CLASS data file
for i in range(6,9):
    filename = join(datadir, 'O-094.F-9307A-2015-2015-01-1{}.apex'.format(i))
    try:
        spectra2, header2, _ = read_class(filename, sourcename=args.source,
                                          telescope='AP-H201-'+args.tel)
        # , line=args.line)
        spectra += spectra2
        header += header2
    except TypeError:
        continue

pd.options.display.max_rows = 999
df = pd.DataFrame(header)

print(df.XLINE.unique())
# print(df.XSOURC.unique())
# print(df.XTEL.unique())
# obslog(df[df['date'] <= 2015.046])
# obslog(df)
ephemeris(df)

# change </> to get observations from first or second night
mask = ((df['OBSDATE'] <= 57039.2) &
        df['XLINE'].str.startswith(args.line.encode('utf-8')))
#         df['XSOURC'].str.startswith(args.source.encode('utf-8')) &
#         df['XTEL'].str.contains(args.tel.encode('utf-8')))

# select spectra for this condition
sp = [s for m, s in zip(mask, spectra) if m]

# frequencies agree within 1 Hz
# freq_list = [frequency(df[mask].iloc[i,:], imagfreq=False) for i in range(len(df[mask]))]
# print(freq_list)
# print(df[mask].iloc[0,:])
freq = frequency(df[mask].iloc[0,:], imagfreq=False)
print(len(freq))
print(freq[0]-freq[-1])
# convert to velocity scale
vel = gildas.vel(freq, freq0[args.line])

ra_list = df[mask]['LAM'].values
dec_list = df[mask]['BET'].values
# LSR to Helio from packages/class/lib/precess.f90
ra_apex = coords.Angle("18h03m50.24s").radian
dec_apex = coords.Angle("30d00m16.8s").radian
solar_velocity = 20.0
# mean observing time
mjd = df[mask]['OBSDATE'].mean()
obsdate = Time(mjd, format='mjd')
exposure = df[mask]['TIME'].sum()
# site elevation, longitude and latitude
alt = 5105
lat = coords.Latitude((-23, 0, 20.8), unit=u.degree)
lon = coords.Longitude((-67, 45, 33.0), wrap_angle=180*u.degree, unit=u.degree)
v = []

for ra, dec, date in zip(ra_list, dec_list, df[mask]['OBSDATE'].values):
    c = coords.FK5(ra=ra*u.radian, dec=dec*u.radian, equinox='J2000')
    # convert from 2000 to 1900 equinox
    c_1900 = c.transform_to(coords.FK5(equinox='J1900'))
    x = c_1900.ra.radian
    y = c_1900.dec.radian
    vlsr = solar_velocity * (np.cos(ra_apex)*np.cos(dec_apex)*np.cos(x)*np.cos(y) +
              np.sin(ra_apex)*np.cos(dec_apex)*np.sin(x)*np.cos(y) +
              np.sin(dec_apex)*np.sin(y))
    velcorr = helcorr(lon, lat, alt, coords.Angle(ra*u.radian),
                   coords.Angle(dec*u.radian), date).to(u.km/u.s).value
    v.append(vlsr + velcorr)

# vel -= v

# convert heliocentric to geocentric velocity


velcorr = [helcorr(lon, lat, alt, coords.Angle(ra*u.radian),
                   coords.Angle(dec*u.radian), i).to(u.km/u.s).value for i in
           df[mask]['OBSDATE'].values]
print(velcorr)

# vel -= velcorr.to(u.km/u.s)

# calculate ephemeris using JPL Horizons
horizons.set_discreteepochs(2400000.5 + df[mask]['OBSDATE'].values)
horizons.get_ephemerides(-7)

print(horizons['delta_rate'])

# horizons.set_discreteepochs(2400000.5+mjd)
# horizons.get_ephemerides(-7)
# mean_delta_rate = horizons['delta_rate']

# vel -= mean_delta_rate

vel_list = [vel - v[i] - horizons['delta_rate'][i] for i in
            range(len(horizons['delta_rate']))]
vel, y = gildas.averagen(vel_list, sp)
# y = np.mean(sp, axis=0)

vel += .2

# convert topocentric velocity to frequency
freq = gildas.freq(vel, freq0[args.line])

rest_frequency = df[mask].iloc[0,:]['RESTF']
# factor from Dirk Mudders
k_doppler = -12e3 / (df[mask].iloc[0,:]['IMAGE']/rest_frequency - 1) / rest_frequency
print(rest_frequency, k_doppler)

# Fit the data using a Gaussian
init = {'v': dict(amplitude=.3, stddev=1.,
                  bounds={'mean': (-.4,.4), 'stddev': (0.1,1.), 'amplitude': (0.,2.)}),
        'f': dict(amplitude=.3, mean=freq0[args.line]+0.012, stddev=.001)}

g_init = models.Gaussian1D(**init[args.x])
fit_g = fitting.LevMarLSQFitter()

conn = sqlite3.connect('Q2.db', detect_types=sqlite3.PARSE_DECLTYPES)
c = conn.cursor()

if args.x == 'f':
    c.execute('''SELECT freq from lamda WHERE molecule like ?''', (args.line+'%',))
    g = fit_g(g_init, freq, y)
    freqs = np.array(c.fetchall()).flatten()
    for i in freqs:
        print(i)
        plt.axvline(x=i, ls='dotted')
    xlim = (np.min(freqs)-.02, np.max(freqs)+.02)
    mask = (freq >= xlim[0]) & (freq <= xlim[1])
    # plt.axvline(x=g.mean, ls='dashed')
    # plt.plot(freq, g(freq))
    print(freq0[args.line] - g.mean)
    plt.xlabel('$f$ [GHz]')
    x = freq[mask]
    # round down length to even number
    n = (len(freq[mask]) // args.rebin) * args.rebin
    xx = freq[mask][:n].reshape(n//args.rebin, args.rebin).mean(axis=1)
    yy = y[mask][:n].reshape(n//args.rebin, args.rebin).mean(axis=1)
    print("{} kHz {} kHz".format(np.median(freq[:-1]-freq[1:])*1e6,
                                 np.median(xx[:-1]-xx[1:])*1e6))
    np.savetxt(join(expanduser('~/project/Q2/paper/ascii/'),
                    'Q2_{0}_{1}_{2}_f.txt'.format(args.line, args.tel,
                                                obsdate.iso[:10])),
               np.c_[xx, yy])
    plt.plot(xx, yy, drawstyle='steps-mid')
    plt.xlim(xlim)
else:
    mask = (vel >= -10) & (vel <= 10)
    g = fit_g(g_init, vel[mask], y[mask])
    plt.plot(vel[mask], y[mask], drawstyle='steps-mid')
    plt.plot(vel[mask], g(vel[mask]))
    plt.axvline(x=0, ls='dotted')
    plt.axvline(x=g.mean, ls='dashed')
    for i in (265.8861886, 265.8864999):
        vel_hfs = gildas.vel(i, freq0[args.line])
        plt.axvline(x=vel_hfs, color='r')
    plt.axvspan(g.mean-3*g.stddev, g.mean+3*g.stddev, alpha=0.3)
    plt.xlim(-10, 10)
    plt.xlabel('$f$ [GHz]')
    plt.xlabel('$v$ [km s$^{-1}$]')
    mask = (vel >= -10) & (vel <= 10)
    np.savetxt(join(expanduser('~/project/Q2/paper/ascii/'),
                    'Q2_{0}_{1}_{2}.txt'.format(args.line, args.tel,
                                                obsdate.iso[:10])),
               np.c_[vel[mask], y[mask]])

    rms = gildas.rms(y, vel, lim=(5, 100))
    intens = gildas.intens(y, vel, lim=(g.mean-3*g.stddev, g.mean+3*g.stddev), rmserror=rms)
    print(intens, g, rms)
    # insert line area into table
    # c.execute('''INSERT into obs2 (line, backend, source, date, exposure,
    #           nscans, rms, intens, intens_e)
    #           SELECT lamda.id, backends.id, ?, ?, ?, ?, ?, ?, ?
    #           FROM lamda, backends WHERE lamda.molecule like ? and backends.backend=?''',
    #           (args.source.replace('^', ''), obsdate.datetime, exposure,
    #            len(sp), np.asscalar(rms)) +
    #           intens + (args.line+'%', 'AP-H201-'+args.tel))
    # conn.commit()
    # vshift = gildas.vshift(y, vel, lim=args.lim)
plt.ylabel('$T_{\mathrm{mB}}$ [K]')
plt.title("{0} {1}".format(args.line, obsdate.iso))
plt.savefig(join(expanduser('~/project/Q2/wiki/figures/'),
                 'Q2_{0}_{1}_{2}_{3}.png'.format(args.line, args.x, args.tel,
                                                 obsdate.iso[:10])))
plt.show()

if 'CH3OH' in args.line and args.x == 'f':
    # find blended lines
    freqs.sort()
    df = freqs[1:]-freqs[:-1]
    ind = np.where(df < 0.002)[0]

    for i in range(len(freqs)):
        if i in ind+1:
            continue
        vel = gildas.vel(freq, freqs[i])
        mask = (vel >= -10) & (vel <= 10)
        if i in ind:
            voff = (freqs[i] - freqs[i+1])/freqs[i]*constants.c * 1e-3
            g = fit_g(models.Gaussian1D(**init['v'], mean=0., fixed={'mean': True})+
                    models.Gaussian1D(**init['v'], mean=voff, fixed={'mean': True}),
                    vel[mask], y[mask])
        else:
            g = fit_g(models.Gaussian1D(**init['v'], mean=0.), vel[mask], y[mask])
        plt.plot(vel[mask], y[mask], drawstyle='steps-mid')
        plt.axhline(y=0, alpha=0.3)
        print(g, obsdate)
        if i in ind:
            plt.axvline(x=g.mean_0, ls='dashed')
            plt.axvline(x=g.mean_1, ls='dashed')
            plt.plot(vel[mask], g(vel[mask]))
            # if i != 3:
        else:
            if g.amplitude > 0.04:
                plt.plot(vel[mask], g(vel[mask]))
                plt.axvline(x=g.mean, ls='dashed')
            else:
                plt.axvline(x=0, ls='dashed')
        plt.xlim(-10, 10)
        plt.xlabel('$f$ [GHz]')
        plt.xlabel('$v$ [km s$^{-1}$]')
        plt.title("CH3OH {0} GHz".format(freqs[i]))
        plt.savefig(join(expanduser('~/project/Q2/wiki/figures/'),
                        'Q2_CH3OH_{0:02d}_{1}_{2}.png'.format(i, args.tel,
                                                            obsdate.iso[:10])))
        plt.show()

        if i not in ind:
            rms = gildas.rms(y, vel, lim=(-200, 300))
            intens = gildas.intens(y, vel, lim=(g.mean-3*g.stddev, g.mean+3*g.stddev), rmserror=rms)
            print(i, intens, g, rms)
            if np.abs(intens[0]/intens[1]) < 2: continue
            # insert line area into table
            # c.execute('''INSERT into obs2 (line, backend, source, date, exposure,
            #         nscans, rms, intens, intens_e)
            #         SELECT lamda.id, backends.id, ?, ?, ?, ?, ?, ?, ?
            #         FROM lamda, backends WHERE lamda.molecule like ? and backends.backend=?
            #         and lamda.freq=?''',
            #         (args.source.replace('^', ''), obsdate.datetime, exposure,
            #         len(sp), np.asscalar(rms)) +
            #         intens + (args.line+'%', 'AP-H201-'+args.tel, freqs[i]))
            # conn.commit()

conn.close()
