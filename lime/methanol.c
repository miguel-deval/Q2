#include "lime.h"

double rh = 1.3; /* AU */
double betawater = 1.042e-5/pow(1.3, 2);
double beta = 1.32e-5/pow(1.3, 2);
double vexp = 800;
double Qwater = 7.5e29;
double Q = 1.8e28;
double tkin = 70.;

/******************************************************************************/

void
input(inputPars *par, image *img){
/*
 * Basic parameters. See cheat sheet for details.
 */
  par->radius		= 1e9;
  par->minScale	   	= 1e3;
  par->pIntensity    	= 1000;
  par->sinkPoints    	= 1000;
  par->moldatfile[0] 	= "a-ch3oh.dat";
  /* par->moldatfile[1] 	= "e-ch3oh.dat"; */

  par->outputfile 	= "populations_ach3oh.pop";
  par->binoutputfile 	= "restart.pop";
  par->gridfile		= "grid.vtk";

  par->collPartIds[0]   = 1;
  par->nMolWeights[0]   = 1.0;

  par->lte_only = 1;

/* 
 * Definitions for image #0. Add blocks for additional images.
 */
  img[0].nchan		= 1000;	  // Number of channels
  img[0].freq 		= 241.81e9;// center frequency
  img[0].bandwidth 	= 1.2e8;  // with of spectral axis
  /* img[0].nchan		= 101;	  // Number of channels */
  /* img[0].velres  	= 30;	  // resolution in m/s */
  /* img[0].trans		= 151; */
  img[0].pxls		= 1000;	  // Pixels per dimension
  img[0].imgres		= 0.4;	  // Resolution in arc seconds
  img[0].distance	= 0.5*AU; // source distance in m
  img[0].unit		= 0;	  // 0:Kelvin 1:Jansky/pixel 2:SI 3:Lsun/pixel 4:tau
  img[0].filename	= "image_ach3oh_lte.fits"; // Output filename
}

/******************************************************************************/

double
electron_temperature(double rcs, double r){
  double Te;
  double Tmax = 1e4;
  if (r < rcs) {
    Te = tkin;
  }
  else if (r > 2*rcs) {
    Te = Tmax;
  }
  else {
    Te = tkin + (Tmax - tkin)*(r/rcs-1);
  }
  return Te;
}

void
density(double x, double y, double z, double *density){
/*
 * Define variable for radial coordinate
 */
  double r, xre, xne, rcs, Rrec, krec, Te, kion, ne;

  const double rMin = 1e3; /* This cutoff should be chosen smaller than par->minScale but greater than zero (to avoid a singularity at the origin). */

  /*
   * Calculate radial distance from origin
   */
  r=sqrt(x*x+y*y+z*z);
  if(r<rMin)
    r = rMin; /* Just to prevent overflows at r==0! */

  density[0] = Qwater/(4*3.14159265358979323846*pow(r, 2)*vexp)*exp(-r*betawater/vexp);
  /* xre = 1.; */
  /* xne = 1.; */
  /* kion = 4.1e-7/pow(rh, 2); /1* s-1 *1/ */
  /* rcs = 1.125e6*xre*pow(Qwater*1e-29, .75); /1* m *1/ */
  /* Rrec = 3.2e6*xre*sqrt(Qwater*1e-29); /1* m *1/ */
  /* Te = electron_temperature(rcs, r); */
  /* krec = sqrt(300./Te)*7e-13; /1* s-1 m3 *1/ */
  /* if (r > 1e4) { */
  /*   ne = xne*sqrt(Qwater*kion/vexp/krec)/rh*pow(Te/300., .15)*\ */
	/* (Rrec/pow(r, 2))*(1.-exp(-r/Rrec)) + 5e6/pow(rh, 2); */
  /* } */
  /* else { */
  /*   ne = xne*sqrt(Qwater*kion/vexp/krec)/rh*pow(Te/300., .15)*\ */
	/* (Rrec/pow(1e4, 2))*(1.-exp(-1e4/Rrec)) + 5e6/pow(rh, 2); */
  /* } */

  /* density[1] = ne; */
}

/******************************************************************************/

void
temperature(double x, double y, double z, double *temperature){
  temperature[0] = tkin;
  /* temperature[1] = tkin; */
}

/******************************************************************************/

void
abundance(double x, double y, double z, double *abundance){
  double r;
  r=sqrt(x*x+y*y+z*z);
  /* Relative abundace wrt water density */
  abundance[0] = Q/Qwater*exp(r/vexp*(betawater-beta));
  /* abundance[1] = Q/Qwater*exp(r/vexp*(betawater-beta)); */
}

/******************************************************************************/

void
doppler(double x, double y, double z, double *doppler){
  *doppler = 100.;
}

/******************************************************************************/

void
velocity(double x, double y, double z, double *vel){
/*
 * Variables for spherical coordinates
 */
  double phi, theta;
/*
 * Transform Cartesian coordinates into spherical coordinates
 */
  theta=atan2(sqrt(x*x+y*y),z);
  phi=atan2(y,x);
/*
 * Vector transformation back into Cartesian basis
 */
  vel[0]=vexp*sin(theta)*cos(phi);
  vel[1]=vexp*sin(theta)*sin(phi);
  vel[2]=vexp*cos(theta);
}
