#!/usr/bin/python

import sqlite3
import pandas as pd
from datetime import datetime, timedelta
from os.path import join, expanduser

def dt_to_fracday(dt):
    """Convert dt to fractional day
    """
    frac = dt.day + (dt-datetime(dt.year, dt.month, dt.day))/timedelta(1)
    mmdd = "{0}/{1:.3f}".format(dt.month, frac)
    return mmdd


paperdir = expanduser('~/project/Q2/paper/')

conn = sqlite3.connect('Q2.db', detect_types=sqlite3.PARSE_DECLTYPES)
c = conn.cursor()

# c.execute('''SELECT molecule, freq, transition, backends.backend, date,
#           exposure, rms, intens, intens_e
#           FROM obs, lines, backends
#           WHERE lines.id=obs.line AND obs.backend=backends.id''')

# data = c.fetchall()

df = pd.read_sql('''SELECT molecule, species, freq, upper, lower,
                 backends.backend, date, exposure, rms, intens, intens_e
                 FROM obs2, lamda, backends
                 WHERE lamda.id=obs2.line
                 AND obs2.backend=backends.id''', conn)

conn.close()

# df['backend'] = df['backend'].str.replace('AP-H201-X20', '')
df['molecule'] = '\\ce{' + df['molecule'] + '}'
df['upper'] = '$' + df['upper'] + '$'
df['lower'] = '$' + df['lower'] + '$'
df['exposure'] = df['exposure']/60.
df['mmdd'] = df['date'].apply(dt_to_fracday)
# df['intens_err'] = '{0:.3f} {1:.3f}'.format(df['intens'], df['intens_e'])
df['freq'] = df['freq'].map(lambda x: '{:3.5f}'.format(x))

print(df.columns)
cols = ['molecule', 'species', 'freq', 'upper', 'lower', 'mmdd',
        'exposure_mean', 'rms_mean', 'intens_err', 'percent']

# average data for XFFTS1 and XFFTS2
group_cols = ['molecule', 'species', 'freq', 'upper', 'lower', 'mmdd']
df2 = df.groupby(group_cols).agg(['mean', 'std']).reset_index()
df2.columns = ['_'.join(col).strip('_') for col in df2.columns.values]

print(df2)

df2['percent'] = (df2['intens_std']/df2['intens_mean']).map(lambda x: '{:2.1f}'.format(x*100))

df2['intens_err'] = df2['intens_mean'].map(lambda x: '{:.3f}'.format(x)) + '\pm' + \
    df2['intens_e_mean'].map(lambda x: '{:.3f}'.format(x))

df2 = df2.sort_values(by=['mmdd', 'freq'])[cols]

df2.to_csv(join(paperdir, "table.tex"), header=False, index=False, sep="&",
           line_terminator="\\\\\n", quotechar=" ", float_format='%1.3f')
