#!/usr/bin/python
"""Calculate rotational diagram"""

import os
import argparse
import sqlite3
import jpl
import numpy as np
from scipy import constants
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import pandas as pd

def performFit(E, N, sigma, outfile='rotdiagram.txt'):
    """Linear fit to rotational diagram"""
    (a, b), pcov = curve_fit(fitfunc, E, np.log(N), sigma=sigma)
    print("T_rot = {0} +- {1}K".format(-1/a, np.sqrt(pcov[0,0])/a/a))
    print("N/Z = {0:e} +- {1:e}cm-2".format(np.exp(b), np.exp(b)*np.sqrt(pcov[1,1])))
    # write linear fit to text file
    j = np.array([30, 100]) # K
    np.savetxt(os.path.join(figsdir, outfile),
            np.transpose((j, np.exp(a*j+b))))
    return a, b

def fitfunc(x, a, b):
    return a*x + b

parser = argparse.ArgumentParser()
parser.add_argument('-t', '--tel', default='AP-H201-X201',
                    choices=('AP-H201-X201', 'AP-H201-X202'),
                    help='spectrometer')
parser.add_argument('-d', '--date', default='2015-01-17%',
                    choices=('2015-01-17%', '2015-01-18%'),
                    help='spectrometer')
args = parser.parse_args()

conn = sqlite3.connect('Q2.db')

df = pd.read_sql('''SELECT l.freq, o.intens, o.intens_e
        FROM obs2 as o, lamda as l, backends as b
        WHERE l.id=o.line and b.id=o.backend and l.molecule like 'CH3OH%' and
        b.backend like ? and o.source='C2014Q2' and o.date like ?
        ORDER BY l.freq''', conn, params=(args.tel, args.date))
conn.close()

df = df[df['intens']/df['intens_e'] > 2]
# convert to SI
df['intens'] *= 1e3 # K m/s
df['intens_e'] *= 1e3 # K m/s


# use gu and Aul from JPL
tag = 32003
methanol = jpl.JPLmol(tag)

idx = np.vectorize(methanol.findfreq)(df.freq.values*1e3)
Aul = methanol.Aul()[idx]
# energy of the upper level
Eu = methanol.trans['ELO'][idx]*1e2*constants.c + df.freq.values*1e9
# convert to K
Eu *= constants.h/constants.k
gu = methanol.trans['GUP'][idx]
Nu = df.intens.values*8*np.pi*(df.freq.values*1e9)**2*constants.k / (constants.h*constants.c**3*Aul)

Nu /= gu
# convert to CGS
Nu *= 1e-4 # cm-2
# rms error
sigma = Nu*df.intens_e.values/df.intens.values

figsdir = '/home/miguel/project/Q2/paper/ascii/'
np.savetxt(os.path.join(figsdir, 'rot_ch3oh_{}_{}.txt'.format(args.tel,
                                                              args.date[:-1])),
           np.transpose((Eu, Nu, sigma)))

a, b = performFit(Eu, Nu, sigma, outfile='rotdiag_{}_{}.txt'.format(args.tel,
                                                                    args.date[:-1]))

plt.errorbar(Eu, Nu, yerr=sigma, fmt=None, marker=None)
# not_blend = [0,1,2,7,8]
# plt.scatter(Eu[not_blend], Nu[not_blend])
plt.scatter(Eu, Nu)
plt.plot(Eu, np.exp(a*Eu+b))
plt.gca().set_yscale("log")
plt.show()
