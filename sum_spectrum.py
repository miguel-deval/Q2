#!/usr/bin/python

"""
Interpolate and sum synthetic spectra
"""

import numpy as np
import matplotlib.pyplot as plt

freq, spec = np.loadtxt('lime/image_ach3oh_0.txt', unpack=True)
freq2, spec2 = np.loadtxt('lime/image_ech3oh_0.txt', unpack=True)

X_new = np.linspace(241.69, 241.92, 5000)
# arrays have to be reversed
y = np.interp(X_new, freq[::-1], spec[::-1]) + np.interp(X_new, freq2[::-1], spec2[::-1])

np.savetxt('lime/ch3oh.txt', np.transpose((X_new, y)))

plt.plot(freq,spec)
plt.plot(freq2,spec2)
plt.plot(X_new,y)
plt.show()
