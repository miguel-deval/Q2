#!/usr/bin/python

from scipy import constants
import numpy as np

hfs = np.array([265884.8912, 265886.1886, 265886.4339, 265886.4999, 265886.9793, 265888.5221])

vel = (hfs[2] - hfs[::-1])/hfs[2]*constants.c*1e-3

print(','.join('{0:1.4f}'.format(i) for i in vel))
