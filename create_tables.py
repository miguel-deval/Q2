#!/usr/bin/python

import sqlite3
from astroquery.splatalogue import Splatalogue
from astroquery.lamda import Lamda
import astropy.units as u
import numpy as np
import pandas as pd
import re

conn = sqlite3.connect('Q2.db', detect_types=sqlite3.PARSE_DECLTYPES)

lamda_mol = {'CO': ('co', '', 230, 231),
             'H2CO': ('oh2co-h2', '', 225, 226),
             'HCN': ('hcn', '', 265, 266),
             'aCH3OH': ('a-ch3oh', 'A', 241.6, 242),
             'eCH3OH': ('e-ch3oh', 'E', 241.6, 242)}

c = conn.cursor()

# Create tables
# c.execute('''CREATE TABLE lines
#             (id integer primary key, molecule text, freq real, transition text)''')
# c.execute('''CREATE TABLE backends
#             (id integer primary key, backend text)''')
# c.execute('''CREATE TABLE obs
#             (id integer primary key,
# 			source text, date timestamp,
#             intens real, intens_e real, rms real,
#             vshift real, vshift_e real,
#             nscans int, exposure real,
#             line integer references lines(id),
#             backend integer references backends(id))''')
# c.execute('''CREATE TABLE obs2
#             (id integer primary key,
# 			source text, date timestamp,
#             intens real, intens_e real, rms real,
#             vshift real, vshift_e real,
#             nscans int, exposure real,
#             line integer references lamda(id),
#             backend integer references backends(id))''')
# c.execute('''CREATE TABLE prodrates
#             (id integer primary key, offset real, lp real, vexp real, tkin real,
#             q real, q_e real, obs integer references obs(id))''')

# # Search line frequencies in Splatalogue
# mol = Splatalogue.query_lines(225*u.GHz,226*u.GHz, chemical_name=' H2CO ',
#                               only_NRAO_recommended=True).to_pandas()
# mol = mol.append(Splatalogue.query_lines(265*u.GHz,266*u.GHz, chemical_name=' HCN ',
#                               only_NRAO_recommended=True).to_pandas())
# mol = mol.append(Splatalogue.query_lines(230*u.GHz,231*u.GHz, chemical_name=' CO ',
#                               only_NRAO_recommended=True).to_pandas())
# mol = mol.append(Splatalogue.query_lines(241.5*u.GHz,242*u.GHz, chemical_name=' CH3OH ',
#                               only_NRAO_recommended=True).to_pandas())
# for i, row in mol.iterrows():
#     if 'OSU' in row['Species']: continue
#     if not np.isfinite(row['Meas Freq-GHz']): continue
#     c.execute('''INSERT into lines (molecule, freq, transition) VALUES (?,?,?)''',
#               row[['Species', 'Meas Freq-GHz', 'Resolved QNs']].values)

# for backend in ('AP-H201-X201', 'AP-H201-X202'):
#     c.execute('''INSERT into backends (backend) VALUES (?)''', (backend,))

# c.execute('''CREATE TABLE lamda
#             (id integer primary key,
#              molecule text, species text, freq real, upper text, lower text)''')

for k,(mol,species,f0,f1) in lamda_mol.items():
    print(mol, species, f0, f1)
    collrates, radtransitions, enlevels = Lamda.query(mol=mol)
    radtrans = radtransitions.to_pandas()
    levels = enlevels.to_pandas()
    trans = radtrans[(radtrans.Frequency > f0) & (radtrans.Frequency < f1)]
    lev_cols = ['Level', 'J']
    trans = pd.merge(trans, levels[lev_cols], left_on="Upper", right_on="Level")
    trans = pd.merge(trans, levels[lev_cols], left_on="Lower", right_on="Level")
    trans['molecule'] = re.sub('[ae]', '', k)
    trans['species'] = species
    trans.rename(columns={'Frequency': 'freq', 'J_x': 'upper', 'J_y': 'lower'}, inplace=True)
    if mol == "hcn":
        trans['upper'] = trans['upper'].str.replace('[" 0]', '')
        trans['lower'] = trans['lower'].str.replace('[" 0]', '')
    trans['upper'] = trans['upper'].str.replace(r'(\d)(_)?(-?\d)?_?(\d)?', r'\1\2{\3\4}')
    trans['lower'] = trans['lower'].str.replace(r'(\d)(_)?(-?\d)?_?(\d)?', r'\1\2{\3\4}')
    print(trans)

    cols = ['molecule', 'species', 'freq', 'upper', 'lower']
    trans[cols].to_sql(con=conn, name='lamda', index=False, if_exists='append')

conn.commit()
conn.close()
