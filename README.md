SPICE Kernels
-------------

The following SPICE kernels that are available from the NAIF webpage at JPL
are required to be present in the kernel directory.  SpiceyPy wrappers for the
SPICE Toolkit are used to calculate the ephemeris.

- [de432s.bsp](http://naif.jpl.nasa.gov/pub/naif/generic_kernels/spk/planets/de432s.bsp)
- [naif0012.tls](http://naif.jpl.nasa.gov/pub/naif/generic_kernels/lsk/naif0012.tls)
- [pck00010.tpc](http://naif.jpl.nasa.gov/pub/naif/generic_kernels/pck/pck00010.tpc)

Using callhorizons instead to get the ephemeris.
