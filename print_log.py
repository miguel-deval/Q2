#!/usr/bin/python

from pyspeckit.spectrum.readers.read_class import read_class
from os.path import join, abspath, expanduser
import argparse
import pandas as pd
from astropy.time import Time
from datetime import datetime, timedelta
import callhorizons

def mjd_to_fracday(mjd):
    """Convert MJD to fractional day
    """
    dt = Time(mjd, format='mjd').to_datetime()
    frac = dt.day + (dt-datetime(dt.year, dt.month, dt.day))/timedelta(1)
    mmdd = "{0}/{1:.3f}".format(dt.month, frac)
    return mmdd


def horizons(mjd):
    """Ephemeris from Horizons"""
    comet = callhorizons.query("C/2014 Q2")
    comet.set_discreteepochs(mjd + 2400000.5)
    # APEX site
    comet.get_ephemerides(-7)
    return pd.DataFrame({'r': comet['r'], 'delta': comet['delta'],
                     'alpha': comet['alpha']})


def fwhm(freq):
    """APEX FWHM
    http://www.apex-telescope.org/telescope/

    Parameters
    ----------
    freq : float
        frequency in GHz
    """
    fwhm = 7.8 * (800 / freq)
    return '{0:2.1f}'.format(fwhm)


# parse command-line arguments
parser = argparse.ArgumentParser()
parser.add_argument('-t', '--tel', choices=('X201', 'X202'), default='X201',
                    help='Telescope')
parser.add_argument('-l', '--line', default='HCN',
                    choices=('HCN', 'CO', 'H2CO', 'H2CO(3-2)', 'CH3OH'),
                    help='Line name')
parser.add_argument('-s', '--source', default='^C2014Q2', help='Source name (default is ^C2014Q2)')
args = parser.parse_args()

spectra = []
header = []
datadir = expanduser('~/project/Q2/data/')
paperdir = expanduser('~/project/Q2/paper/')

# read CLASS data file
for i in range(6,9):
    filename = join(datadir, 'O-094.F-9307A-2015-2015-01-1{}.apex'.format(i))
    try:
        spectra2, header2, _ = read_class(filename, sourcename=args.source,
                                          telescope='AP-H201-'+args.tel)
        # , line=args.line)
        spectra += spectra2
        header += header2
    except TypeError:
        continue

pd.options.display.max_rows = 999
df = pd.DataFrame(header)

df['BLOCK'] = (df.XLINE.shift(1) != df.XLINE).astype(int).cumsum()

# Date & $r_\mathrm{h}$ & $\Delta$ & $\phi$ & Species & freq & $N$ & Int & Beam FWHM                \\
df2 = df[['XLINE', 'OBSDATE', 'RESTF', 'BLOCK', 'TIME']].groupby(['BLOCK',
'XLINE', 'RESTF'], as_index=False).agg({'OBSDATE': ['mean', 'count'], 'TIME': 'sum'})
df2.columns = list(map(''.join, df2.columns.values))
df2.loc[:,'TIMEsum'] /= 60
df2.loc[:,'RESTF'] /= 1e3
df2['XLINE'] = df2['XLINE'].str.decode("utf-8").replace({'(\(3-2\))? *': ''}, regex=True)
df2['XLINE'] = '\\ce{' + df2['XLINE'] + '}'
df2['mmdd'] = df2['OBSDATEmean'].apply(mjd_to_fracday)
df2['fwhm'] = df2['RESTF'].apply(fwhm)
df2 = df2.merge(horizons(df2['OBSDATEmean'].values), left_index=True, right_index=True)
df2['alpha2'] = df2['alpha'].apply('{0:2.2f}'.format)
df2['integration'] = df2['TIMEsum'].apply('{0:2.1f}'.format)
print(df2['XLINE'].values)
columns = ['mmdd', 'r', 'delta', 'alpha2', 'XLINE', 'integration', 'fwhm']
# Print latex tabular environment
df2[columns].to_csv(join(paperdir, "log.tex"), header=False, index=False,
                    sep="&", line_terminator="\\\\\n", quotechar=" ",
                    float_format='%1.4f')
